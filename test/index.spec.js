const chai = require('chai');
const sinon = require('sinon');
const expect = chai.expect;
chai.use(require('sinon-chai'));
const queryString = '/status-email?accountId=1&accountsEndpoint=account&usersEndpoint=user&emailEndpoint=sendGrid';
const proxyquire = require('proxyquire').noCallThru();


describe('index', () => {
	let index;
	const mocks = {};
	beforeEach(() => {
		mocks.request = {
			get: sinon.stub().resolves({}),
			post: sinon.stub()
		}
		index = proxyquire('../index', {
			'request-promise-native': mocks.request
		});
	});
	describe('when sending an email to all account owners', () => {
		it('retrieves a list of users associated with the given account', async () => {
			await index.sendAccountOwnerEmail(queryString);
			expect(mocks.request.get).to.have.been.calledWithMatch('/account/1');
		});
		it('retrieves user information for the users associated with the account', async () => {
			mocks.request.get = sinon.stub().resolves({}).onFirstCall().resolves({ users: [1, 2, 3] });
			await index.sendAccountOwnerEmail(queryString);
			expect(mocks.request.get).to.have.been.calledWithMatch('/user/1')
			expect(mocks.request.get).to.have.been.calledWithMatch('/user/2')
			expect(mocks.request.get).to.have.been.calledWithMatch('/user/3')
		});
		it('sends the account status and user information through the email endpoint', async () => {
			mocks.request.get = sinon.stub()
				.onFirstCall().resolves({ status: 'ACTIVE', users: [1, 2, 3] })
				.onSecondCall().resolves({ userId: 1, firstName: 'Biff', lastName: 'Tannen', email: 'biff@biffsautodetailing.com' })
				.onThirdCall().resolves({ userId: 2, firstName: 'John', lastName: 'Cena', email: 'john@cena.com' })
				.onCall(3).resolves({ userId: 3, firstName: 'Bob', lastName: 'Hope', email: 'bob@hope.com' })
			await index.sendAccountOwnerEmail(queryString);
			expect(mocks.request.post).to.have.been.calledWithMatch(
				'/sendGrid',
				{
					body: {
						accountStatus: 'ACTIVE',
						users: [
							{ firstName: 'Biff', email: 'biff@biffsautodetailing.com' },
							{ firstName: 'John', email: 'john@cena.com' },
							{ firstName: 'Bob', email: 'bob@hope.com' }
						]
					}
				}
			);
		});
	});
});
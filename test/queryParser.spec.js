const expect = require('chai').expect;
const queryParser = require('../lib/queryParser');
const queryString = '/status-email?accountId=8675309&accountsEndpoint=account&usersEndpoint=user&emailEndpoint=sendGrid';

describe('queryParser', () => {
	describe('when parsing a query string', () => {
		it('returns an empty map if a string without query values is given', () => {
			const actual = queryParser.parseQueryString('');
			const actual2 = queryParser.parseQueryString(null);
			expect(actual).to.be.empty;
			expect(actual2).to.be.empty;
		});
		it('returns a map of keys to values', () => {
			const expected = {
				accountId: '8675309',
				accountsEndpoint: 'account',
				usersEndpoint: 'user',
				emailEndpoint: 'sendGrid'
			}
			const actual = queryParser.parseQueryString(queryString);
			expect(actual).to.deep.equal(expected);
		});
	});
});
module.exports = {
	parseQueryString
};

function parseQueryString(queryString) {
	const map = {};
	if (!queryString)
		return map;
	const queryStartingIndex = queryString.indexOf('?') + 1;
	const query = queryString.substr(queryStartingIndex);
	const queryPairs = query.split('&');
	queryPairs.forEach(queryPair => {
		const pair = queryPair.split('=');
		if (pair.length === 2 && pair[0] && pair[1])
			map[pair[0]] = pair[1];
	});
	return map;
}
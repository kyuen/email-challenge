const queryParser = require('./lib/queryParser');
const request = require('request-promise-native');

const queryString = '/status-email?accountId=8675309&accountsEndpoint=account&usersEndpoint=user&emailEndpoint=sendGrid';
const BASE_URL = 'https://safe-beyond-14033.herokuapp.com';

async function sendAccountOwnerEmail(qs) {
	const queryMap = queryParser.parseQueryString(qs);
	if (queryMap.accountsEndpoint && queryMap.accountId) {
		const accountUrl = `${BASE_URL}/${queryMap.accountsEndpoint}/${queryMap.accountId}`;
		const account = await request.get(accountUrl, { json: true });
		const users = await Promise.all(getUserPromises(queryMap.usersEndpoint, account.users));
		await sendEmailAsync(queryMap.emailEndpoint, account.status, users);
	}
}

function getUserPromises(endpoint, users) {
	if (!endpoint || !users || users.length === 0) return [];
	return users.map((user) => {
		const userUrl = `${BASE_URL}/${endpoint}/${user}`;
		return request.get(userUrl, { json: true });
	});
}

async function sendEmailAsync(endpoint, accountStatus, users) {
	if (!endpoint || !accountStatus || !users || users.length === 0) return;
	const emailUrl = `${BASE_URL}/${endpoint}`
	const body = {
		accountStatus: accountStatus,
		users: users.map((user) => {
			return { firstName: user.firstName, email: user.email }
		})
	};
	return request.post(emailUrl, { body, json: true });
}

sendAccountOwnerEmail(queryString);

module.exports = { sendAccountOwnerEmail };